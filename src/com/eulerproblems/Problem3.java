package com.eulerproblems;

public class Problem3 extends Problem {
    public double run(double n) {
        double max = 0;
        double bound = Math.sqrt(n);
        for (int i = 3; i < bound; i += 2) {
            if (n % i == 0 && isPrime(i)) {
                if (i > max)
                    max = i;
            }
        }
        return max;
    }

    private boolean isPrime(double n) {
        if (n % 2 == 0)
            return false;

        double mid = (n / 2) % 2 == 0 ? (n / 2) - 1 : n / 2;
        for (int i = 3; i < mid; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}
