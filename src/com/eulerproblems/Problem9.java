package com.eulerproblems;

public class Problem9 extends Problem {
    public int run(int n) {
        int a, b, c;
        int squareN = (int) Math.sqrt(n);
        for (int i = 1; i <= squareN; i++) {
            for (int j = i + 1; j <= squareN; j++) {
                a = (j * j) - (i * i);
                b = 2 * j * i;
                c = (j * j) + (i * i);
                if ((a + b + c) == n)
                    return a * b * c;
            }
        }

        return -1;
    }
}
