package com.eulerproblems;

import java.util.Scanner;

public class Main {
    private static Problem p;

    public static void main(String[] args) throws Exception {
        Scanner stdin = new Scanner(System.in);
        String choice;
        do {
            printOptions();
            choice = stdin.nextLine();
            execute(choice);
        } while (!choice.equalsIgnoreCase("q"));

        System.exit(0);
    }

    private static void execute(String choice) {
        switch (choice) {
            case "1":
                runProblem1();
                break;
            case "2":
                runProblem2();
                break;
            case "3":
                runProblem3();
                break;
            case "4":
                runProblem4();
                break;
            case "5":
                runProblem5();
                break;
            case "6":
                runProblem6();
                break;
            case "7":
                runProblem7();
                break;
            case "8":
                runProblem8();
                break;
            case "9":
                runProblem9();
                break;
            case "10":
                runProblem10();
                break;
            case "all":
                runAll();
                break;
        }
    }

    private static void runAll() {
        System.out.print("Problem 1: ");
        runProblem1();
        System.out.print("Problem 2: ");
        runProblem2();
        System.out.print("Problem 3: ");
        runProblem3();
        System.out.print("Problem 4: ");
        runProblem4();
        System.out.print("Problem 5: ");
        runProblem5();
        System.out.print("Problem 6: ");
        runProblem6();
        System.out.print("Problem 7: ");
        runProblem7();
        System.out.print("Problem 8: ");
        runProblem8();
        System.out.print("Problem 9: ");
        runProblem9();
        System.out.print("Problem 10: ");
        runProblem10();
    }

    private static void runProblem1() {
        p = new Problem1();
        System.out.println(p.run(1000));
    }

    private static void runProblem2() {
        p = new Problem2();
        System.out.println(p.run(4000000));
    }

    private static void runProblem3() {
        p = new Problem3();
        System.out.printf("%.0f%n",p.run(600851475143.0));
    }

    private static void runProblem4() {
        p = new Problem4();
        System.out.println(p.run());
    }

    private static void runProblem5() {
        p = new Problem5();
        System.out.println(p.run());
    }

    private static void runProblem6() {
        p = new Problem6();
        System.out.println(p.run(100));
    }

    private static void runProblem7() {
        p = new Problem7();
        System.out.printf("%.0f%n", p.run(10001.0));
    }

    private static void runProblem8() {
        p = new Problem8();
        System.out.printf("%.0f%n", p.run(1000.0));
    }

    private static void runProblem9() {
        p = new Problem9();
        System.out.println(p.run(1000));
    }

    private static void runProblem10() {
        p = new Problem10();
        System.out.printf("%.0f%n", p.run(2000000.0));
    }

    private static void printOptions() {
        System.out.println("Enter number for desired problem, all to run all problems, h for this dialog, q or Q to exit.");
    }
}
