package com.eulerproblems;


public class Problem10 extends Problem {

    public double run(double d) {
        double result = 2;
        for (int i = 3; i < d; i += 2) {
            if (isPrime(i))
                result += i;
        }

        return result;
    }

    private boolean isPrime(double n) {
        if (n % 2 == 0)
            return false;

        for (int i = 3; i*i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}
