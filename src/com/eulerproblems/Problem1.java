package com.eulerproblems;

public class Problem1 extends Problem {
    public int run(int n) {
        int result = 0;
        for (int i = 0; i <= (n - 1) / 3; i++)
            result += i * 3;
        for (int i = 0; i <= (n - 1) / 5; i++)
            result += i * 5;
        for (int i = 0; i <= (n - 1) / 15; i++)
            result -= i * 15;
        return result;
    }
}
