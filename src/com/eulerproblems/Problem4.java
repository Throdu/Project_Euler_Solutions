package com.eulerproblems;

public class Problem4 extends Problem {
    public int run() {
        int result = 0;
        for (int i = 100; i <= 999; i++) {
            for (int j = 100; j <= 999; j++) {
                if (isPalindrome(i * j) && (i * j) > result)
                    result = (i * j);
            }
        }
        return result;
    }

    public boolean isPalindrome(int n) {
        char[] nums = Integer.toString(n).toCharArray();
        int low = 0;
        int high = nums.length - 1;
        while (low <= high) {
            if (nums[low] != nums[high]) {
                return false;
            }
            low++;
            high--;
        }

        return true;
    }
}
