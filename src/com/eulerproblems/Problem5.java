package com.eulerproblems;

public class Problem5 extends Problem {
    public int run() {
        int i = 1;
        while (true) {
            if (divisible(20 * i))
                return 20 * i;

            i++;
        }
    }

    public boolean divisible(int n) {
        for (int i = 19; i > 1; i--) {
            if (n % i != 0)
                return false;
        }
        return true;
    }
}
