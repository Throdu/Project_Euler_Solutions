package com.eulerproblems;

public class Problem2 extends Problem {
    public int run(int n) {
        int previous2 = 1;
        int previous1 = 1;
        int result = 0;

        while (previous1 + previous2 < n) {
            int temp = previous1 + previous2;
            if (temp % 2 == 0)
                result += temp;
            previous2 = previous1;
            previous1 = temp;
        }
        return result;
    }
}

