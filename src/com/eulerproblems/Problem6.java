package com.eulerproblems;

public class Problem6 extends Problem {
    public int run(int n) {
        double sumOfSquares = 0;
        double squareOfSum = 0;

        for (int i = 1; i <= n; i++) {
            sumOfSquares += Math.pow(i, 2);
            squareOfSum += i;
        }

        squareOfSum = Math.pow(squareOfSum, 2);
        return (squareOfSum > sumOfSquares) ? (int) (squareOfSum - sumOfSquares) : (int) (sumOfSquares - squareOfSum);
    }
}
