package com.eulerproblems;

import java.util.ArrayList;

public class Problem7 extends Problem {
    public double run(double d) {
        ArrayList<Double> primes = new ArrayList<>();
        for (double i = 1; primes.size() <= d; i++) {
            if (isPrime(i))
                primes.add(i);
        }
        return primes.get((int) d - 1);
    }

    private boolean isPrime(double n) {
        if (n % 2 == 0)
            return false;

        double mid = (n / 2) % 2 == 0 ? (n / 2) - 1 : n / 2;
        for (int i = 3; i < mid; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}
