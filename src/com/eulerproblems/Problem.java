package com.eulerproblems;

public abstract class Problem {
    public int run(int n){
        return 0;
    }
    public double run(double n){
        return 0.0;
    }
    public int run(){
        return 0;
    }
}
